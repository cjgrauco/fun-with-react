import React, { Component } from 'react';
import './App.css';

const FRONT_PAGE = 'http://hn.algolia.com/api/v1/search?tags=front_page';

//TODO: 
// search field
// footer info
// view comments
// view stories that are internal posts


class App extends Component {


  constructor(props){
    super(props);
    this.state = {
      result: null
    }
    this.submitSearch = this.submitSearch.bind(this);
    this.fetchFrontPage = this.fetchFrontPage.bind(this);

  }

  submitSearch(){ 
    console.log("submitSearch()");

  }

  fetchFrontPage(){
    console.log('fetchFrontPage()');
    fetch(FRONT_PAGE)
    .then(response => response.json())
    .then(result => this.setFP(result));
  }

  setFP(result){
    this.setState({ result });
    console.log("setFP()");
    console.log("result.hits.length:" +  result.hits.length);
    
    for(let value of result.hits){
      if (!value.url){
        console.log("No URL:");
        console.log(value);
      }
    }
  }

  componentDidMount(){
    this.fetchFrontPage();

  }


// called:
// during mount process
// when the component is updated
// when props are read from the component above
  render() {
    const {result} = this.state;
    return (
      <div className="App">
        <Header>
          <button className="btn-search" onClick={this.submitSearch}>Search</button>
          <h1>hackernews.</h1>
        </Header>
        <Body>
        {result &&
        <FPTable
        result={result.hits}
        > </FPTable>
        }
        </Body> 

        <Footer>Johan Graucob, 2017</Footer>
      </div>
    );
  }
}

export default App;

const Footer = ({ children }) =>
        <div className="app-footer">
        <div className="app-footer-txt">
          { children }        
        </div>
        </div>

const Header = ({ children }) =>
<div className="app-header">
  { children }
</div>

const Body = ({ children }) =>
<div className="app-body">{ children } </div>


const FPTable = ({ result }) =>
<div className="fp-table">
{ result.map(item =>
  <div key={item.objectID} className="fpt-row">
  <span style={{ width: '80%' }}>
  <a href={item.url} target="_blank">{item.title}</a></span>
  <span style={{width: '20%'}}>{item.author}</span>
  </div>


)}
</div>

